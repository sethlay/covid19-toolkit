import React, { useState } from 'react';
import Form from './components/Form';
import Cards from './components/Cards';
import CardsForCruds from './components/CardsForCruds.js';
// import { Button, CardBody, Table, } from 'reactstrap';
import Numbers from './components/Data.js';
import Food from './components/Food';
import './styles/CovidApp.css';

const CovidApp = () => {


  const [showForm, setShowForm] = useState(false);
  const [showFormSymptoms, setShowFormSymptomps] = useState(false);
  const [showFormPrevention, setShowFormPrevention] = useState(false);
  const [showFormHospital, setShowFormHospital] = useState(false);
  const [showFormFood, setShowFormFood] = useState(false);//to be removed
  const [showFormEmergency, setShowFormEmergency] = useState(false);
  const [showAddRestaurant, setShowAddRestaurant] = useState(false);

  const [restaurants, setRestaurants] = useState([

    {
      name: "Jollibee",
      number: "#8-7000"
    }, {
      name: "Burger King",
      number: "#2-22-22"
    }, {
      name: "Chowking",
      number: "#9-8888"
    }, {
      name: "Greenwich",
      number: "#5-55-55"
    }, {
      name: "Mang Inasal",
      number: "#7-3333"
    }, {
      name: "Red Ribbon",
      number: "#8-7777"
    }, {
      name: "Shakeys",
      number: "7777-7777"
    }, {
      name: "Max's",
      number: "8888-9000"
    }

  ]);

  const toggleForm = () => {
    setShowForm(false);

  }

  const openForm = () => {
    setShowForm(true);
  }

  const toggleForm1 = () => {
    setShowFormSymptomps(false);
  }

  const openForm1 = () => {
    setShowFormSymptomps(true);
  }

  const toggleForm2 = () => {
    setShowFormPrevention(false);
  }

  const openForm2 = () => {
    setShowFormPrevention(true);
  }

  const toggleForm3 = () => {
    setShowFormHospital(false);
  }

  const openForm3 = () => {
    setShowFormHospital(true);
  }

  const toggleForm4 = () => {
    setShowAddRestaurant(false);
  }

  const openForm4 = () => {
    setShowAddRestaurant(true);
  }

  const toggleForm5 = () => {
    setShowFormEmergency(false);
  }

  const openForm5 = () => {
    setShowFormEmergency(true);
  }

  const toggleForm6 = () => { //to be removed
    setShowAddRestaurant(false); //to be removed
  }

  const openForm6 = () => { //to be removed
    setShowAddRestaurant(true); //to be removed
  }

  const addRestaurant = (name, number) => {
    let newRestaurants = [];


    newRestaurants = [...Numbers, {
      name: name,
      number: number
    }];


    setRestaurants(newRestaurants);
    setShowAddRestaurant(false);
    console.log(newRestaurants);
  }

  const deleteRestaurant = (index) => {
    const newRestaurant = restaurants.filter((restaurant, qIndex) => {
      return qIndex !== index
    });
    setRestaurants(newRestaurant);
  }

  return (

    <div
      className='container'
    >
      <CardsForCruds
        openForm3={openForm3}
        openForm4={openForm4}
        openForm5={openForm5}
      />

      <Cards
        openForm={openForm}
        openForm1={openForm1}
        openForm2={openForm2}

      />

      <Form
        showForm={showForm}
        toggleForm={toggleForm}
        showFormSymptoms={showFormSymptoms}
        toggleForm1={toggleForm1}
        showFormPrevention={showFormPrevention}
        toggleForm2={toggleForm2}
        showFormHospital={showFormHospital}
        toggleForm3={toggleForm3}
        showFormFood={showFormFood}
        toggleForm4={toggleForm4}
        showFormEmergency={showFormEmergency}
        toggleForm5={toggleForm5}
        showAddRestaurant={showAddRestaurant}
        toggleForm6={toggleForm6}
        addRestaurant={addRestaurant}
        restaurants={restaurants}
      />


      {/* <Food
        restaurants={restaurants}
        deleteRestaurant={deleteRestaurant}
      /> */}


    </div >
  )
}

export default CovidApp;