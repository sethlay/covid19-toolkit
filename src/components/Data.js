const Numbers = [
  {
    name: "Jollibee",
    number: "#8-7000"
  }, {
    name: "Burger King",
    number: "#2-22-22"
  }, {
    name: "Chowking",
    number: "#9-8888"
  }, {
    name: "Greenwich",
    number: "#5-55-55"
  }, {
    name: "Mang Inasal",
    number: "#7-3333"
  }, {
    name: "Red Ribbon",
    number: "#8-7777"
  }, {
    name: "Shakeys",
    number: "7777-7777"
  }, {
    name: "Max's",
    number: "8888-9000"
  }
]

export default Numbers;