import React from 'react';
import { Card, CardHeader, CardBody, Button } from 'reactstrap';
import CarouselExample from './CarouselExample';
import CarouselSymptoms from './CarouselSymptoms';
import CarouselPrevention from './CarouselPrevention';
import '../styles/CardImg.css';
import PropTypes from 'prop-types';


const Cards = (props) => {



  return (
    <>

      <div className='col-lg-9'>

        <Card
          className='my-5 mx-1'
        >
          <CardHeader className='cardHeader'>
            <h4
              className='fontStyle'
            >ABOUT COVID-19</h4>
          </CardHeader>
          <CardBody>
            <CarouselExample />
          </CardBody>
          <Button
            onClick={props.openForm}
            className='Button BlackButton'
          >Learn More</Button>
        </Card>

        <Card
          className='my-5 mx-1'
        >
          <CardHeader className='cardHeader'>
            <h4
              className='fontStyle'
            >SYMPTOMS</h4>
          </CardHeader>
          <CardBody>
            <CarouselSymptoms />
          </CardBody>
          <Button
            onClick={props.openForm1}
            className='Button BlackButton'
          >Learn More</Button>
        </Card>

        <Card
          className='my-5 mx-1'
        >
          <CardHeader className='cardHeader'>
            <h4
              className='fontStyle'
            >PREVENTION</h4>
          </CardHeader>
          <CardBody>
            <CarouselPrevention />
          </CardBody>
          <Button
            onClick={props.openForm2}
            className='Button BlackButton'
          >Learn More</Button>
        </Card>

      </div>
    </>
  )
}

Button.propTypes = {
  onClick: PropTypes.func,
  className: PropTypes.string
}

export default Cards;