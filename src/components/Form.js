import React, { useState } from 'react';
import { Modal, ModalHeader, ModalBody, Button, Input, FormGroup, Label } from 'reactstrap';
import '../styles/Form.css';

const Form = (props) => {

  const [name, setName] = useState('')
  const [number, setNumber] = useState('')

  return (
    <>

      <Modal
        isOpen={props.showForm}
        toggle={props.toggleForm}
      >
        <ModalHeader
          toggle={props.toggleForm}
          className='modalHeaderForm'
        >
          What is COVID-19?
          </ModalHeader>
        <ModalBody
          className='modalBodyForm'
        >
          <p className='px-2 text-justify'>
            Coronavirus disease (COVID-19) is an infectious disease caused by a new virus.
            The disease causes respiratory illness (like the flu) with symptoms such as a cough, fever, and in more severe cases, difficulty breathing.
            <hr />
            You can protect yourself by washing your hands frequently, avoiding touching your face, and avoiding close contact (1 meter or 3 feet) with people who are unwell.
          </p>

        </ModalBody>
        <Button className='ButtonForm' href='https://en.wikipedia.org/wiki/2019%E2%80%9320_coronavirus_pandemic_by_country_and_territory'>Let's visit here!</Button>
      </Modal>
      <Modal
        isOpen={props.showFormSymptoms}
        toggle={props.toggleForm1}
      >
        <ModalHeader
          toggle={props.toggleForm1}
          className='modalHeaderForm'
        >What are the common symptoms?</ModalHeader>
        <ModalBody
          className='modalBodyForm'
        >
          <p className='px-2 text-justify'>
            People may be sick with the virus for 1 to 14 days before developing symptoms. The most common symptoms of coronavirus disease (COVID-19) are fever, tiredness, and dry cough. Most people (about 80%) recover from the disease without needing special treatment.
          <br />
            <br />
More rarely, the disease can be serious and even fatal. Older people, and people with other medical conditions (such as asthma, diabetes, or heart disease), may be more vulnerable to becoming severely ill.
          <hr />
          People may experience:
          <br />
            <ul className='px-4'>
              <li>cough</li>
              <li>fever</li>
              <li>tiredness</li>
              <li>difficulty breathing (severe cases)</li>
            </ul>
          </p>
        </ModalBody>
        <Button
          href='https://fightcovid.app/?fbclid=IwAR0BORRt9GAvaMkh6VRMOURTq-tD5WdpIHjlOgVq3d-QmLqeIJw8KYWyi3k'
          className='ButtonForm'
        >Better check yourself!</Button>
      </Modal>
      <Modal
        isOpen={props.showFormPrevention}
        toggle={props.toggleForm2}
      >
        <ModalHeader
          toggle={props.toggleForm2}
          className='modalHeaderForm'
        >
          Let us Prevent COVID-19!
        </ModalHeader>
        <ModalBody
          className='modalBodyForm'
        >
          <h6 className='px-2 text-justify'>There’s currently no vaccine to prevent coronavirus disease (COVID-19).</h6>
          <hr />
          <p className='px-2 text-justify'>
            You can protect yourself and help prevent spreading the virus to others if you:
            <br />
            <br />
            Do:
            <ul className='px-4'>
              <li>Wash your hands regularly for 20 seconds, with soap and water or alcohol-based hand rub</li>
              <li>Cover your nose and mouth with a disposable tissue or flexed elbow when you cough or sneeze</li>
              <li>Avoid close contact (1 meter or 3 feet) with people who are unwell</li>
              <li>Stay home and self-isolate from others in the household if you feel unwell</li>
            </ul>
            <br />
            <br />
            Don't:
            <br />
            <br />
            <ul className='px-4'>
              <li>Touch your eyes, nose, or mouth if your hands are not clean</li>
            </ul>
          </p>
        </ModalBody>
      </Modal>
      <Modal
        isOpen={props.showFormHospital}
        toggle={props.toggleForm3}
      >
        <ModalHeader
          toggle={props.toggleForm3}
          className='modalHeaderForm'
        >Assigned Hospitals by the Government</ModalHeader>
        {/* <ModalBody
          className='modalBodyForm'
        > */}
        {/* <p>ModalBody</p> */}
        {/* </ModalBody> */}
        <Button
          className='ButtonForm'
          href='https://endcov.ph/hospitals/'
        >Click here and Stay safe!</Button>
      </Modal>
      <Modal
        isOpen={props.showFormFood}
        toggle={props.toggleForm4}
      >
        <ModalHeader
          toggle={props.toggleForm4}
          className='modalHeaderForm'
        >Food Delivery Hotline</ModalHeader>
        <ModalBody
          className='modalBodyForm'
        >
          <>
            {props.restaurants.map((restaurant, index) => (
              <tr
                key={index}
              >
                <td>
                  <p>Restaurant #{index + 1}</p>
                  <p className='text-danger'>{restaurant.name}</p>
                  <p className='text-secondary'>{restaurant.number}</p>

                </td>
              </tr>

            ))}
          </>
        </ModalBody>
      </Modal>
      <Modal
        isOpen={props.showFormEmergency}
        toggle={props.toggleForm5}
      >
        <ModalHeader
          toggle={props.toggleForm5}
          className='modalHeaderForm'
        >Emergency Hotline</ModalHeader>
        <ModalBody
          className='modalBodyForm'
        >Covid19</ModalBody>
      </Modal>

      {/* FOR CRUDS */}

      <Modal
        isOpen={props.showAddRestaurant} //check when modal will appear
        toggle={props.toggleForm6} //function that will run when you close the modal
      >
        <ModalHeader
          toggle={props.toggleForm6} // to close the form outside not just on x button
        >Add your restaurant</ModalHeader>
        <ModalBody>
          <FormGroup>
            <Label>Restaurant Name:</Label>
            <Input
              placeholder='Type your restaurant name'
              type='text'
              onChange={(e) => setName(e.target.value)}
            />
          </FormGroup>
          <FormGroup>
            <Label>Phone Number :</Label>
            <Input
              placeholder='Type your restaurant phone number'
              onChange={(e) => setNumber(e.target.value)}
              type='formatPhoneNumber'
            />
          </FormGroup>

        </ModalBody>
        <Button
          className='ButtonForm'
          onClick={() => props.addRestaurant(name, number)}
        >Add</Button>
      </Modal>


    </>
  )
}

export default Form;