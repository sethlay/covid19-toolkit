import React from 'react';
import { Button } from 'reactstrap';


const Food = (props) => {
  return (
    <>
      <div className='flex-column text-center justify-content-center align-items-center'>
        {
          props.restaurants.map((restaurant, index) => (
            <tr
              key={index}
            >


              <td
                className='border'
              >
                {/* <p>Restaurant #{index + 1}</p> */}
                <p className='text-danger'>{restaurant.name}</p>
                <p className='text-secondary'>{restaurant.number}</p>

              </td>
              <Button
                color='danger'
                onClick={() => props.deleteRestaurant(index)} // onclick to send data should use anonymous function ()=>
                className='my-1'
              >Delete</Button>
              <Button
                color='primary'
                // onClick={() => props.deleteQuestion(index)} // onclick to send data should use anonymous function ()=>
                className='my-1'
              >Edit</Button>
            </tr>


          ))}

      </div>

    </>
  )
}

export default Food;