import React, { useState } from 'react';
import { Card, CardHeader, CardBody, Button } from 'reactstrap';
import '../styles/CardsForCruds.css';
import PropTypes from 'prop-types';
import Food from './Food';
import Numbers from './Data.js';
import Form from './Form';


const Cards = (props) => {

  const [showAddRestaurant, setShowAddRestaurant] = useState(false);

  const [restaurants, setRestaurants] = useState([

    {
      name: "Jollibee",
      number: "#8-7000"
    }, {
      name: "Burger King",
      number: "#2-22-22"
    }, {
      name: "Chowking",
      number: "#9-8888"
    }, {
      name: "Greenwich",
      number: "#5-55-55"
    }, {
      name: "Mang Inasal",
      number: "#7-3333"
    }, {
      name: "Red Ribbon",
      number: "#8-7777"
    }, {
      name: "Shakeys",
      number: "7777-7777"
    }, {
      name: "Max's",
      number: "8888-9000"
    }

  ]);

  const toggleForm4 = () => {
    setShowAddRestaurant(false);
  }

  const openForm4 = () => {
    setShowAddRestaurant(true);
  }

  const toggleForm6 = () => { //to be removed
    setShowAddRestaurant(false); //to be removed
  }

  const openForm6 = () => { //to be removed
    setShowAddRestaurant(true); //to be removed
  }

  const addRestaurant = (name, number) => {
    let newRestaurants = [];


    newRestaurants = [...Numbers, {
      name: name,
      number: number
    }];


    setRestaurants(newRestaurants);
    setShowAddRestaurant(false);
    console.log(newRestaurants);
  }

  const deleteRestaurant = (index) => {
    const newRestaurant = restaurants.filter((restaurant, qIndex) => {
      return qIndex !== index
    });
    setRestaurants(newRestaurant);
  }

  return (
    <>

      <div className='col-lg-3'>

        <Card className='my-5 mx-2'>
          <CardHeader className='cardHeaderCruds'>
            Assigned Hospitals:
          </CardHeader>
          <CardBody
            className='cardBodyCruds'
          >
            <ul
              className='px-3'
            >
              <li>
                <p>University of the Philippines - Philippine General Hospital</p>
                <hr />
                <p>(02) 8554 8400</p>
                <p>Taft Ave, Ermita, Manila, 1000 Metro Manila</p>
              </li>
              <hr
                className='hrStyle'
              />
              <li>
                <p>Lung Center of the Philippines</p>
                <hr />
                <p>(02) 8924 6101</p>
                <p>Quezon Ave, Diliman, Quezon City, Metro Manila</p>
              </li>
            </ul>
          </CardBody>
          <Button
            onClick={props.openForm3}
            className='ButtonCruds ColorButton'
          >Upates</Button>
        </Card>



        <Card className='my-5 mx-2'>
          <CardHeader className='cardHeaderCruds'>
            Food Delivery Hotline
          </CardHeader>
          <CardBody
            className='cardBodyCruds'
          >

            <Food
              restaurants={restaurants}
              deleteRestaurant={deleteRestaurant}
            />
          </CardBody>
          < Form
            showAddRestaurant={showAddRestaurant}
            toggleForm6={toggleForm6}
            addRestaurant={addRestaurant}
            restaurants={restaurants}
          />
          <Button
            className='ButtonCruds ColorButton'
            onClick={() => setShowAddRestaurant(true)}
          >Be our business partner. Add your restaurant</Button>

        </Card>

        <Card className='my-5 mx-2'>
          <CardHeader className='cardHeaderCruds'>
            Emergency Hotline
          </CardHeader>
          <CardBody
            className='cardBodyCruds'
          >
            <p>Call 911</p>
          </CardBody>
          <Button
            onClick={props.openForm5}
            className='ButtonCruds ColorButton'
          >Updates</Button>
        </Card>

      </div>
    </>
  )
}

Button.propTypes = {
  onClick: PropTypes.func,
  className: PropTypes.string
}

export default Cards;